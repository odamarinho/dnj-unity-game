﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemie : MonoBehaviour
{
    public float attack_recoil;
    public float attack_range;
    public int damage;

    float counter = 0;

    public Animation anim;

    private void Start()
    {
        anim.Play("Idle");
    }

    //public Animation anim;

    // Update is called once per frame
    void Update()
    {
        Collider[] player = Physics.OverlapSphere(transform.position, attack_range, (1 << 9));

        if (player.Length > 0)
        {
            counter += Time.deltaTime;
            transform.LookAt(player[0].gameObject.transform);

            if (counter > attack_recoil)
            {
                GameController.game_controller.give_damage(damage);
                anim.Play("Attack");

                counter = 0f;
            }
        }

        else counter = 0f;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("COLIDIU!!");

        if (other.gameObject.tag == "spell")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, attack_range);
    }
}
