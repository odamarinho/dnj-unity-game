﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public Text hp_text;
    public Text mana_text;
    public PlayerStats player_stats;

    public static GameController game_controller;

    public bool player_protected = false;

    private void Start()
    {
        game_controller = this;
    }

    public void give_damage(int damage)
    {
        player_stats.hp -= damage;
        update_hp();

        if(player_stats.hp <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void update_hp()
    {
        if (player_stats.hp < player_stats.full_hp / 2) hp_text.color = Color.red;
        else hp_text.color = Color.black;

        hp_text.text = "HP: " + player_stats.hp.ToString();
    }

    public void update_mana()
    {
        if (player_stats.mana < player_stats.full_mana / 2) mana_text.color = Color.red;
        else mana_text.color = Color.black;

        mana_text.text = "Mana: " + player_stats.mana.ToString();
    }
}
