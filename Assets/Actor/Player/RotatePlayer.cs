﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayer : MonoBehaviour
{
    public Animation anim;
    
    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            Vector3 mouse = Input.mousePosition;
            float z = Vector3.Distance(mouse, transform.position);
            Vector3 mouse_world_pos = Camera.main.ScreenToWorldPoint(new Vector3(mouse.x, mouse.y, Camera.main.transform.position.y));

            transform.LookAt(mouse_world_pos);

            anim.Play("attack_short_001");
        }

        if(anim["attack_short_001"].normalizedTime > 0.99f)
        {
            anim.Play("idle_combat");
        }

    }
}
