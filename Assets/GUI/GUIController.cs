﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    public Text hp;
    public Text mana;
    public Text rt;

    public PlayerStats ps;
    public SpellController sc;

    public List<Sprite> spells_sprites;

    public Image eq_spell1;
    public Image eq_spell2;
    public Image eq_spell3;

    private void Start()
    {
        hp.text = "HP: " + ps.hp.ToString();
        mana.text = "Mana: " + ps.mana.ToString();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            sc.equiped_spell = 0;
            rt.text = "EQUIPED";
        }

        else if(Input.GetKeyDown(KeyCode.Alpha2) && eq_spell2 != null)
        {
            sc.equiped_spell = 1;
            rt.text = "                        EQUIPED";
        }

        else if (Input.GetKeyDown(KeyCode.Alpha3) && eq_spell3 != null)
        {
            sc.equiped_spell = 2;
            rt.text = "                                                EQUIPED";
        }
    }
}
