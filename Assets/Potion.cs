﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.tag == "player")
        {
            GameController.game_controller.player_stats.hp = 50;
            GameController.game_controller.player_stats.mana = 50;
            GameController.game_controller.update_hp();
            GameController.game_controller.update_mana();

            Destroy(gameObject);
        }
    }

}
