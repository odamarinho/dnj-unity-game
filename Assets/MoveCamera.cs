﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveCamera : MonoBehaviour
{
    public Transform player;
    Vector3 dis;
    public float follow_speed;
    public float cam_distance;

    private void Start()
    {
        dis = new Vector3(0, cam_distance, 0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Slerp(transform.position, player.position - dis, Time.deltaTime * follow_speed);
    }
}
