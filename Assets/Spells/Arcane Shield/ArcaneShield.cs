﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneShield : Spell
{
    bool conjuring = false;
    public GameObject effect;

    float counter = 0;

    private void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            GameController.game_controller.player_protected = false;

            conjuring = false;
            counter = 0;
            Destroy(conjured_spell);
        }

        if (conjuring)
        {
            if (GameController.game_controller.player_stats.mana < mana_coast)
            {
                GameController.game_controller.player_protected = false;

                conjuring = false;
                counter = 0f;
                Destroy(conjured_spell);
            }

            else if (counter > 1.0f)
            {
                GameController.game_controller.player_stats.mana -= mana_coast;
                GameController.game_controller.update_mana();

                counter = 0f;
            }

            counter += Time.deltaTime;
        }
    }

    public override void Conjure(Transform player)
    {
        GameController.game_controller.player_protected = true;
        GameController.game_controller.player_stats.mana += mana_coast;
        GameController.game_controller.update_mana();

        conjuring = true;
        conjured_spell = Instantiate(effect, transform.position, Quaternion.identity);
        conjured_spell.transform.parent = player.parent;

    }
}
