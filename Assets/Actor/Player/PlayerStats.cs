﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [HideInInspector]
    public int hp;
    [HideInInspector]
    public int mana;

    public int full_hp;
    public int full_mana;

    private void Start()
    {
        hp = full_hp;
        mana = full_mana;
    }
}
