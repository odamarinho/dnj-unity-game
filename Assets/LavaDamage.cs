﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaDamage : MonoBehaviour
{
    bool damage_it = false;
    float count = 0;
    public int lava_damage = 15;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "lava")
        {
            damage_it = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "lava")
        {
            damage_it = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (damage_it)
        {
            count += Time.deltaTime;

            if (count > 1f)
            {
                if (!GameController.game_controller.player_protected)
                {
                    GameController.game_controller.give_damage(lava_damage);
                    GameController.game_controller.update_hp();
                }

                count = 0f;
            }
        }
    }


}
