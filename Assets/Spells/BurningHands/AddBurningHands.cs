﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddBurningHands : MonoBehaviour
{
    public Sprite spell_icon;
    public Image spell2_image;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.tag == "player")
        {
            spell2_image.sprite = spell_icon;

            BurningHands current = GetComponent<BurningHands>();
            BurningHands bh = SpellController.spell_controller.gameObject.AddComponent<BurningHands>();
            bh.effect = current.effect;
            bh.mana_coast = current.mana_coast;
            bh.raio = current.raio;
            bh.recoil = current.recoil;
            bh.damage_interval = current.damage_interval;

            SpellController.spell_controller.add_spell(bh, 1);
            Destroy(gameObject);
        }
    }
}
