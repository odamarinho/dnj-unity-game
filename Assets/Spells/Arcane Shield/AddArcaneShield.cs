﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddArcaneShield : MonoBehaviour
{
    public Sprite spell_icon;
    public Image spell3_image;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.tag == "player")
        {
            spell3_image.sprite = spell_icon;

            ArcaneShield arcs = SpellController.spell_controller.gameObject.AddComponent<ArcaneShield>();
            ArcaneShield current = GetComponent<ArcaneShield>();

            arcs.mana_coast = current.mana_coast;
            arcs.effect = current.effect;

            SpellController.spell_controller.add_spell(arcs, 2);
            Destroy(gameObject);
        }
    }
}
