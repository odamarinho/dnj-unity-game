﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellController : MonoBehaviour
{
    public List<Spell> spells;
    public static SpellController spell_controller;
    public Transform mesh;
    public int equiped_spell;

    private bool can_conjure = true;
    private float time_passed = 0f;
    private float atual_recoil;

    PlayerStats player_stats;

    // Update is called once per frame
    void Start()
    {
        spell_controller = this;
        equiped_spell = 0;

        //spells = new List<Spell>(GetComponents<Spell>());

        player_stats = mesh.parent.GetComponent<PlayerStats>();

        //Debug.Log(spells.Count);
    }

    private void Update()
    {
        if(!can_conjure)
        {
            time_passed += Time.deltaTime;
            //Debug.Log(time_passed);
            if (time_passed >= atual_recoil)
            {
                time_passed = 0f;
                can_conjure = true;
            }
        }

        if(Input.GetMouseButtonDown(0) && (equiped_spell == 1 || equiped_spell == 2))
        {
            ConjureSpell();
        }

        if (Input.GetMouseButtonUp(0) && equiped_spell == 0)
        {
            ConjureSpell();
        }
    }

    public void ConjureSpell()
    {
        if(can_conjure && player_stats.mana >= spells[equiped_spell].mana_coast)
        {
            can_conjure = false;
            atual_recoil = spells[equiped_spell].recoil;
            spells[equiped_spell].Conjure(mesh);
            player_stats.mana -= spells[equiped_spell].mana_coast;

            GameController.game_controller.update_mana();
        }
    }

    public void add_spell(Spell spell, int index)
    {
        spells[index] = spell;
    }
}
