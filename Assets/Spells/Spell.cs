﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : MonoBehaviour
{
    public int mana_coast;
    public Sprite icon;
    public float recoil;
    public GameObject conjured_spell;

    public virtual void Conjure(Transform player)
    {

    }

    public virtual void Conjure(Transform player, Vector3 target)
    {

    }
}
