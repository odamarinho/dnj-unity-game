﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneMissile : Spell
{
    public GameObject effect;

    public float speed;
    bool conjured = false;

    private void Update()
    {
        if(conjured && conjured_spell != null)
        {
            Debug.Log(conjured_spell.transform.rotation);
            conjured_spell.transform.position += conjured_spell.transform.forward * speed * Time.deltaTime;
        }
    }

    public override void Conjure(Transform player)
    {
        conjured = true;
        Vector3 spell_pos = new Vector3(player.position.x, player.position.y + 5, player.position.z);
        conjured_spell = Instantiate(effect, spell_pos, Quaternion.identity);
        conjured_spell.transform.forward = player.forward;
    }
}
