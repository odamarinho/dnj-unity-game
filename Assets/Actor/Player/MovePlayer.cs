﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float speed;
    public Animation anim;
    public Transform mesh;

    private bool is_attacking = false;
    public Rigidbody rb;

    // Update is called once per frame
    void Update()
    {
        float hor = Input.GetAxisRaw("Horizontal");
        float ver = Input.GetAxisRaw("Vertical");

        if (ver != 0f || hor != 0f)
        {
            Vector3 move = new Vector3(hor, 0, ver);
            if (ver != 0f && hor != 0f) move.Normalize();
            rb.velocity = move * speed * Time.deltaTime;

            if (!anim.IsPlaying("attack_short_001"))
            {
                if (hor != 0) mesh.forward = Vector3.left * -hor;
                if (ver != 0) mesh.forward = Vector3.forward * ver;
                anim.Play("move_forward_fast");
            }
        }

        else if (!is_attacking)
        {
            anim.Play("idle_combat");
            rb.velocity = Vector3.zero;
        }

        if (Input.GetMouseButtonUp(0))
        {
            Vector3 mouse = Input.mousePosition;
            Vector3 mouse_world_pos = Camera.main.ScreenToWorldPoint(new Vector3(mouse.x, mouse.y, 35f));
            mouse_world_pos.y = transform.position.y;
            //Debug.Log(mouse_world_pos);

            mesh.LookAt(mouse_world_pos);

            anim.Play("attack_short_001");

            is_attacking = true;
        }

        if (anim["attack_short_001"].normalizedTime > 0.80f) is_attacking = false;
    }
}
