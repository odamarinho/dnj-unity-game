﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : Spell
{
    public GameObject effect;
    public float speed;

    bool conjured = false;
    Transform player;
    Vector3 target;

    private void Update()
    {
        if (conjured)
        {
            float x_speed = Vector3.Distance(player.position, target);
            float y_speed = 1f / x_speed;

            Vector3 new_pos = new Vector3();
            new_pos.x = conjured_spell.transform.position.x + x_speed;
            new_pos.y = conjured_spell.transform.position.y + y_speed;
            conjured_spell.transform.position += conjured_spell.transform.forward * speed * Time.deltaTime;
        }
    }

    public override void Conjure(Transform player, Vector3 target)
    {
        conjured = true;
        conjured_spell = Instantiate(effect, player.position, Quaternion.identity);
        conjured_spell.transform.forward = player.forward;

        this.player = player;
        this.target = target;
    }
}
