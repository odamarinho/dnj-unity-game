﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningHands : Spell
{
    public float raio = 5f;
    public float damage_interval;

    float counter = 0;
    float passed_time = 0;
    bool conjuring = false;

    Transform player;

    public GameObject effect;

    private void OnDrawGizmos()
    {
        if(conjuring)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(player.position, raio);
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            conjuring = false;
            counter = 0;
            Destroy(conjured_spell);
        }

        if (conjuring)
        {
            if (GameController.game_controller.player_stats.mana < mana_coast)
            {
                conjuring = false;
                Destroy(conjured_spell);
                counter = 0f;
            }

            else if (counter > damage_interval)
            {
                Collider[] targets = Physics.OverlapSphere(player.position, raio, (1 << 8));
                foreach (Collider t in targets)
                {
                    Destroy(t.gameObject);
                }

                GameController.game_controller.player_stats.mana -= mana_coast;
                GameController.game_controller.update_mana();
                counter = 0;

                Debug.Log("ENTROU EM CONJURING");
            }

            counter += Time.deltaTime;
        }
    }

    public override void Conjure(Transform player)
    {
        this.player = player;

        GameController.game_controller.player_stats.mana += mana_coast;
        GameController.game_controller.update_hp();

        conjured_spell = Instantiate(effect, this.player.position, Quaternion.identity);
        conjured_spell.transform.parent = player.parent;
        conjuring = true;
        counter = 0;
    }
}
