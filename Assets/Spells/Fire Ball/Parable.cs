﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Parable : Spell
{
    public Rigidbody bullet;
    public GameObject cursor;
    public LayerMask layer;

    Camera cam;
    
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        //launch_projectile();
    }

    public override void Conjure(Transform player)
    {
        Ray cam_ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(cam_ray, out hit, 100f, layer))
        {
            Vector3 vo = calculate_vel(hit.point, player.transform.position, 1.5f);

            conjured_spell = Instantiate(bullet.gameObject, player.transform.position, Quaternion.identity);
            conjured_spell.GetComponent<Rigidbody>().velocity = vo;

            float prev_x_rot = player.rotation.x;
            float prev_z_rot = player.rotation.z;
            player.transform.LookAt(vo);
            player.rotation = new Quaternion(prev_x_rot, player.rotation.y, prev_z_rot, player.rotation.w);
        }
    }

    Vector3 calculate_vel(Vector3 target, Vector3 origin, float time)
    {
        Vector3 distance = target - origin;
        Vector3 distanceXZ = distance;
        distanceXZ.y = 0f;

        float sy = distance.y;
        float sxz = distanceXZ.magnitude;

        float vxz = sxz / time;
        float vy = sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = distanceXZ.normalized;
        result *= vxz;
        result.y = vy;

        return result;
    }
}
